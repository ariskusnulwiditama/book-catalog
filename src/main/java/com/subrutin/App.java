package com.subrutin;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.subrutin.domain.Author;
import com.subrutin.dto.BookDetailDTO;
import com.subrutin.service.BookService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("application-context.xml");
    	BookService bookService = (BookService)appContext.getBean("bookService");
    	BookDetailDTO bookDetailDTO = bookService.findBookDetailById(2L);
    	System.out.println("book detail: "+bookDetailDTO);
//    	Author authorBean = (Author)appContext.getBean("author");
//    	System.out.println(authorBean);
    }
}
