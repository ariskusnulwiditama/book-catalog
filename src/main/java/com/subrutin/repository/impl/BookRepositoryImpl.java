package com.subrutin.repository.impl;

import java.util.HashMap;
import java.util.Map;

import com.subrutin.domain.Author;
import com.subrutin.domain.Book;
import com.subrutin.repository.BookRepository;

public class BookRepositoryImpl implements BookRepository{
	private Map<Long, Book> bookMap;
	
	
//	public BookRepositoryImpl() {
//		super();
//		bookMap = new HashMap<>();
//		Book book1 = new Book();
//		book1.setId(1L);
//		book1.setTitle("Bumi manusia");
//		book1.setDescription("buku minke");
//		Author author1 = new Author();
//		author1.setId(1L);
//		author1.setName("Pramoedya ananta toer");
//		author1.setBirthDate(-16401L);
//		book1.setAuthor(author1);
//		bookMap.put(book1.getId(), book1);
//	}


	@Override
	public Book findBookById(Long id) {
		return bookMap.get(id);
	}


	public Map<Long, Book> getBookMap() {
		return bookMap;
	}


	public void setBookMap(Map<Long, Book> bookMap) {
		this.bookMap = bookMap;
	}
	
}
